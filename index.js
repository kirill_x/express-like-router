const http = require('http')

const Router = require('./router')

const router = new Router()

const sleep = time => new Promise(resolve => setTimeout(resolve, time))

router.use('*', async (req, res, next) => {
  req.startTime = Number(new Date())
  next()
})

router.use('*', async (req, res, next) => {
  await sleep(1000)
  next()
})

router.use('/ru', (req, res, next) => {
  req.lang = 'ru'
  next()
})

router.use((req, res, next) => {
  req.lang = 'en'
  next()
})

router.get('*', (req, res) => {
  const helloMessage = req.lang === 'ru' ? 'Привет!' : 'Hello!'
  const resultTime = Number(new Date()) - req.startTime
  res.end(helloMessage + ' :: ' + resultTime)
})

http.createServer(router.serve()).listen(3000)
